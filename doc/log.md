# Table of Contents

1.  [Project Log - Zaqar](#orgaaf57ce)
2.  [nebunez](#org8623c4b)
    1.  [#5](#org994a983)
    2.  [#2](#org98b56c6)
    3.  [#1](#orga6133ca)
    4.  [#9](#org7fdff91)


<a id="orgaaf57ce"></a>

# Project Log - Zaqar

<https://gitlab.com/w0rmh0le/zaqar>


<a id="org8623c4b"></a>

# nebunez


<a id="org994a983"></a>

## #5

-   Create style guide for site
    -   Create Button styles


<a id="org98b56c6"></a>

## #2

-   Create SCHEMA and TABLE scripts for database
-   Create teardown script
-   Tested all scripts


<a id="orga6133ca"></a>

## #1

-   Create database class diagram


<a id="org7fdff91"></a>

## #9

-   Create system sequence diagrams
    -   Consumer
    -   Creator
    -   Moderator
