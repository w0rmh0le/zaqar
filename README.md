# Zaqar

Zaqar is a full-stack template for file-serving websites/apps

## Overview

[consumer sequence diagram](doc/system-consumer.sequence.png)

[creator sequence diagram](doc/system-creator.sequence.png)

[moderator sequence diagram](doc/system-moderator.sequence.png)

## Name origin

In Mesopotamian mythology, Zaqar or Dzakar is the messenger of the god Sin. He relays these messages to mortals through his power over their dreams and nightmares.
