SET FOREIGN_KEY_CHECKS = 0;


-- CREATE SCHEMAS --------------------------------------------------------------
--------------------------------------------------------------------------------

-- z_security ----------
CREATE SCHEMA IF NOT EXISTS z_security
       CHARACTER SET = utf8mb4
       COLLATE = utf8mb4_unicode_ci;

SHOW WARNINGS;

-- z_content ----------
CREATE SCHEMA IF NOT EXISTS z_content
       CHARACTER SET = utf8mb4
       COLLATE = utf8mb4_unicode_ci;

SHOW WARNINGS;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


-- CREATE TABLES ---------------------------------------------------------------
--------------------------------------------------------------------------------

-- z_security ------------------------------------------------------------------

-- account ----------
CREATE TABLE IF NOT EXISTS z_security.account (
       id              BIGINT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
       account_type_id BIGINT(20) NOT NULL,
       username        VARCHAR(50) NOT NULL,
       email           VARCHAR(255) NOT NULL,
       password        VARCHAR(255) NOT NULL,

       CONSTRAINT account_fk_account_type_id
                  FOREIGN KEY (account_type_id)
                  REFERENCES z_security.account_type (id)
                  ON DELETE RESTRICT
                  ON UPDATE CASCADE
) ENGINE = InnoDB;

SHOW WARNINGS;

-- account_type ----------
CREATE TABLE IF NOT EXISTS z_security.account_type (
       id                BIGINT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
       account_type_name VARCHAR(30) NOT NULL
) ENGINE = InnoDB;

SHOW WARNINGS;

-- /z_security -----------------------------------------------------------------


-- z_config --------------------------------------------------------------------

-- content ----------
CREATE TABLE IF NOT EXISTS z_content.content (
       id               BIGINT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
       content_type_id  BIGINT(20) NOT NULL,
       content_state_id BIGINT(20) NOT NULL,
       account_id       BIGINT(20) NOT NULL,
       filepath         VARCHAR(255) NOT NULL,
       title            VARCHAR (50) NOT NULL,
       description      VARCHAR(2000) DEFAULT NULL,

       CONSTRAINT content_fk_content_type_id
                  FOREIGN KEY (content_type_id)
                  REFERENCES z_content.content_type (id)
                  ON DELETE RESTRICT
                  ON UPDATE CASCADE,

       CONSTRAINT content_fk_content_state_id
                  FOREIGN KEY (content_state_id)
                  REFERENCES z_content.content_state (id)
                  ON DELETE RESTRICT
                  ON UPDATE CASCADE,

       CONSTRAINT content_fk_account_id
                  FOREIGN KEY (account_id)
                  REFERENCES z_security.account (id)
                  ON DELETE CASCADE
                  ON UPDATE RESTRICT
) ENGINE = InnoDB;

SHOW WARNINGS;

CREATE TABLE IF NOT EXISTS z_content.content_type (
       id                BIGINT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
       content_type_name VARCHAR(250) NOT NULL
) ENGINE = InnoDB;

SHOW WARNINGS;

CREATE TABLE IF NOT EXISTS z_content.content_state (
       id                 BIGINT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
       content_state_name VARCHAR(50) NOT NULL
) ENGINE = InnoDB;

SHOW WARNINGS;

CREATE TABLE IF NOT EXISTS z_content.tag (
       id       BIGINT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
       tag_name VARCHAR(50) NOT NULL
) ENGINE = InnoDB;

SHOW WARNINGS;

CREATE TABLE IF NOT EXISTS z_content.content_tag (
       content_id BIGINT(20) NOT NULL,
       tag_id     BIGINT(20) NOT NULL,

       CONSTRAINT content_tag_fk_content_id
                  FOREIGN KEY (content_id)
                  REFERENCES z_content.content (id)
                  ON DELETE CASCADE
                  ON UPDATE RESTRICT,

       CONSTRAINT content_tag_fk_tag_id
                  FOREIGN KEY (tag_id)
                  REFERENCES z_content.tag (id)
                  ON DELETE CASCADE
                  ON UPDATE RESTRICT
) ENGINE = InnoDB;

SHOW WARNINGS;

-- /z_config -------------------------------------------------------------------

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;
